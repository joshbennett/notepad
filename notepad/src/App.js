import './App.css';
import { Notepad } from './components/notepad/Notepad';

function App() {
  return (
    <div className="App">
      <h1 id="header">Web Notepad</h1>
      <Notepad />
    </div>
  );
}

export default App;
