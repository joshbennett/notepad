import React from "react";
import { useState, useEffect } from "react";
import "../notepad/Notepad.css";

export const Notepad = () => {
  const [textData, setTextData] = React.useState(
    window.localStorage.getItem("document")
  );

  useEffect(() => {
    loadTextData();
    updateTextData();
    console.log(textData);
    console.log(window.localStorage);
  }, [textData]);

  //   useEffect(() => {}, []);

  const loadTextData = () => {
    window.localStorage.getItem("document");
  };

  const updateTextData = () => {
    let value = document.getElementById("text");
    setTextData(value.value);
    window.localStorage.setItem("document", textData);
  };

  const saveTextData = () => {
    let text = document.getElementById("text").value;
    text = text.replace(/\n/g, "\r\n");
    let blob = new Blob([text], { type: "text/plain" });
    let anchor = document.createElement("a");
    anchor.download = "filename.txt";
    anchor.href = window.URL.createObjectURL(blob);
    anchor.target = "_blank";
    anchor.style.display = "none";
    document.body.appendChild(anchor);
    anchor.click();
    document.body.removeChild(anchor);
  };

  const clearTextData = () => {
    console.log("text cleared");
    window.localStorage.setItem("document", "");
    setTextData("");
    document.getElementById("text").value = "";
  };

  return (
    <div>
      <textarea
        id="text"
        cols={100}
        rows={30}
        autoFocus
        onChange={updateTextData}
        defaultValue={textData}
      ></textarea>
      <div id="controls">
        <button id="save" onClick={saveTextData}>
          Save
        </button>
        <button id="clear" onClick={clearTextData}>
          Clear
        </button>
      </div>
    </div>
  );
};
